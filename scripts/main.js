// alle div-Elemente mit Klasse .cell selektieren
const elSquares = document.querySelectorAll('div.cell');
// durch alle Quadrate iterieren und nummerieren
for (let i = 0; i < elSquares.length; i++) {
  const elSquare = elSquares[i];
  elSquare.textContent = i + 1;
}

// Quadrate in 1. und letzter Zeile selektieren
const elSquares1stLastRow = document.querySelectorAll(
  'div.row:nth-child(1) div.cell, ' + // Quadrate in 1. Zeile
    'div.row:nth-last-child(1) div.cell' // Quadrate in letzter Zeile
);
// durch Quadrate von 1. und letzer Zeile iterieren
// und Klasse yellow hinzufügen
for (let i = 0; i < elSquares1stLastRow.length; i++) {
  const elCell = elSquares1stLastRow[i];
  elCell.classList.add('yellow');
}

// Quadrate in der Mitte selektieren
const elSquaresInTheMiddle = document.querySelectorAll(
  'div.row:nth-child(2) div.cell:nth-child(2), ' + // Quadrat 6
    'div.row:nth-child(2) div.cell:nth-child(3), ' + // Quadrat 7
    'div.row:nth-child(3) div.cell:nth-child(2), ' + // Quadrat 10
    'div.row:nth-child(3) div.cell:nth-child(3)' // Quadrat 11
);
// durch Quadrate in Mitte iterieren und Klasse orange hinzufügen
for (let i = 0; i < elSquaresInTheMiddle.length; i++) {
  const elCell = elSquaresInTheMiddle[i];
  elCell.classList.add('orange');
}

// Quadrate in Ecke selektieren
const elSquaresInTheCorner = document.querySelectorAll(
  'div.row:nth-child(1) div.cell:nth-child(1), ' + // Quadrat 1
    'div.row:nth-child(1) div.cell:nth-child(4), ' + // Quadrat 4
    'div.row:nth-child(4) div.cell:nth-child(1), ' + // Quadrat 12
    'div.row:nth-child(4) div.cell:nth-child(4)' // Quadrat 16
);
// durch Quadrate in Ecke iterieren, Klasse yellow entfernen
// und Klasse red hinzufügen
for (let i = 0; i < elSquaresInTheCorner.length; i++) {
  const elCell = elSquaresInTheCorner[i];
  elCell.classList.remove('yellow');
  elCell.classList.add('red');
  // Alternativ: elCell.setAttribute('class', 'cell red');
}
